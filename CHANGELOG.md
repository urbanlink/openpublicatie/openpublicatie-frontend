## [1.8.2](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.8.1...v1.8.2) (2020-12-05)


### Bug Fixes

* update homepage features ([86e03b8](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/86e03b8f087bf71f135ca01d00b5f85bf6011c37))

## [1.8.2](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.8.1...v1.8.2) (2020-12-05)


### Bug Fixes

* update homepage features ([86e03b8](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/86e03b8f087bf71f135ca01d00b5f85bf6011c37))

## [1.8.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.8.0...v1.8.1) (2020-12-01)


### Bug Fixes

* update create request form ([5077a13](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/5077a136e142ce8c88a3e7b4ec36bd9754b7b98e))

## [1.8.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.8.0...v1.8.1) (2020-12-01)


### Bug Fixes

* update create request form ([5077a13](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/5077a136e142ce8c88a3e7b4ec36bd9754b7b98e))

# [1.8.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.7.1...v1.8.0) (2020-12-01)


### Features

* add google analytics ([fbb93f8](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/fbb93f86f879e5ccedd42b268e9117bc6bf51878))

# [1.8.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.7.1...v1.8.0) (2020-12-01)


### Features

* add google analytics ([fbb93f8](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/fbb93f86f879e5ccedd42b268e9117bc6bf51878))

## [1.7.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.7.0...v1.7.1) (2020-11-29)


### Bug Fixes

* use proper date in notifications list ([e40ee0d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e40ee0d3e7705d99d6148511be7449c9c5778bb1))

## [1.7.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.7.0...v1.7.1) (2020-11-29)


### Bug Fixes

* use proper date in notifications list ([e40ee0d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e40ee0d3e7705d99d6148511be7449c9c5778bb1))

# [1.7.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.6.1...v1.7.0) (2020-11-29)


### Features

* add request status ([6721e5d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6721e5d26a0ae046b9063aeb90aa8af4ce5317bf))

# [1.7.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.6.1...v1.7.0) (2020-11-29)


### Features

* add request status ([6721e5d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6721e5d26a0ae046b9063aeb90aa8af4ce5317bf))

## [1.6.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.6.0...v1.6.1) (2020-11-29)


### Bug Fixes

* various fixes ([f2b2589](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/f2b2589ac0468114fd5496d4e7602a50c2f72be6))

## [1.6.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.6.0...v1.6.1) (2020-11-29)


### Bug Fixes

* various fixes ([f2b2589](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/f2b2589ac0468114fd5496d4e7602a50c2f72be6))

# [1.6.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.9...v1.6.0) (2020-11-28)


### Features

* add community channels to home ([8e76b6d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/8e76b6d28bc5f6ea8e0c07cc6cf9b266dac24dbd))

# [1.6.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.9...v1.6.0) (2020-11-28)


### Features

* add community channels to home ([8e76b6d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/8e76b6d28bc5f6ea8e0c07cc6cf9b266dac24dbd))

## [1.5.9](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.8...v1.5.9) (2020-11-28)


### Bug Fixes

* update request tabs ([d2f4ffe](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/d2f4ffec03585a276a208ae6da1b35ff3e7d409e))

## [1.5.9](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.8...v1.5.9) (2020-11-28)


### Bug Fixes

* update request tabs ([d2f4ffe](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/d2f4ffec03585a276a208ae6da1b35ff3e7d409e))

## [1.5.8](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.7...v1.5.8) (2020-11-27)


### Bug Fixes

* add request page ([97504a4](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/97504a4c95a3e9b87d0fd42ef43828d5cc54b4af))

## [1.5.8](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.7...v1.5.8) (2020-11-27)


### Bug Fixes

* add request page ([97504a4](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/97504a4c95a3e9b87d0fd42ef43828d5cc54b4af))

## [1.5.7](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.6...v1.5.7) (2020-11-23)


### Bug Fixes

* update api url process.env ([c90c887](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/c90c88732275c27e6e54676cb70062901b8a851c))

## [1.5.7](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.6...v1.5.7) (2020-11-23)


### Bug Fixes

* update api url process.env ([c90c887](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/c90c88732275c27e6e54676cb70062901b8a851c))

## [1.5.6](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.5...v1.5.6) (2020-11-23)


### Bug Fixes

* test url ([0426c76](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0426c760d1401eaea185014f77301cd6c4db9867))

## [1.5.6](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.5...v1.5.6) (2020-11-23)


### Bug Fixes

* test url ([0426c76](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0426c760d1401eaea185014f77301cd6c4db9867))

## [1.5.5](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.4...v1.5.5) (2020-11-22)


### Bug Fixes

* update api and slug ([8c05cb7](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/8c05cb7f8f16287d482d09d2a2692539295135bf))

## [1.5.5](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.4...v1.5.5) (2020-11-22)


### Bug Fixes

* update api and slug ([8c05cb7](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/8c05cb7f8f16287d482d09d2a2692539295135bf))

## [1.5.4](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.3...v1.5.4) (2020-11-17)


### Bug Fixes

* create request form ([2045b65](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/2045b65b7698b1c4f7c6fd011208979989f4ec09))
* update home ([6ffe355](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6ffe355f82cbd82c1d148adc584733e3c3395483))
* use ssr to add idtoken to page ([e0a682e](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e0a682e01b5cba1395a3cd23e0f9924aeaabfaf4))

## [1.5.4](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.3...v1.5.4) (2020-11-17)


### Bug Fixes

* create request form ([2045b65](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/2045b65b7698b1c4f7c6fd011208979989f4ec09))
* update home ([6ffe355](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6ffe355f82cbd82c1d148adc584733e3c3395483))
* use ssr to add idtoken to page ([e0a682e](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e0a682e01b5cba1395a3cd23e0f9924aeaabfaf4))

## [1.5.3](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.2...v1.5.3) (2020-11-15)


### Bug Fixes

* update homepage styling ([32e6821](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/32e68213ba04c3f530fb701d36a97543c2a1fe00))

## [1.5.3](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.2...v1.5.3) (2020-11-15)


### Bug Fixes

* update homepage styling ([32e6821](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/32e68213ba04c3f530fb701d36a97543c2a1fe00))

## [1.5.2](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.1...v1.5.2) (2020-11-15)


### Bug Fixes

* update colors ([e954ca2](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e954ca207c72c094c88b04d64fb05c79486a424c))

## [1.5.2](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.1...v1.5.2) (2020-11-15)


### Bug Fixes

* update colors ([e954ca2](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e954ca207c72c094c88b04d64fb05c79486a424c))

## [1.5.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.0...v1.5.1) (2020-11-15)


### Bug Fixes

* convert to typescript ([26d6c64](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/26d6c64f8ed6e542606e123580dddd8e98e82562))
* update fonts ([133fa03](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/133fa03b95e9919c18f6bce42dfd0e5b6156f7b1))
* update lint ([e996662](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e996662c8bab12579093359767f66705b3fe7bb0))
* update logo ([ac10a3d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/ac10a3dd75723aade9e4bab7a550b0c0f6767605))

## [1.5.1](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.5.0...v1.5.1) (2020-11-15)


### Bug Fixes

* convert to typescript ([26d6c64](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/26d6c64f8ed6e542606e123580dddd8e98e82562))
* update fonts ([133fa03](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/133fa03b95e9919c18f6bce42dfd0e5b6156f7b1))
* update lint ([e996662](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e996662c8bab12579093359767f66705b3fe7bb0))
* update logo ([ac10a3d](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/ac10a3dd75723aade9e4bab7a550b0c0f6767605))

# [1.5.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.4.0...v1.5.0) (2020-09-29)


### Bug Fixes

* remove env from now.json ([187f8ed](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/187f8edb66c97540320396ebc8ff17f88ebc14d2))
* update env vars ([fc59bba](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/fc59bbaaa528c6d6e3b9f092c7104fe399938e09))
* update readme ([d17dfe9](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/d17dfe9fb8f109fd976dd69a87c74595eb3d5108))
* **auth:** remove auth0-js dependency ([96efeb5](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/96efeb52df8539db0519598511c26fb29f43edee))
* **auth:** update env vars ([2d3379c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/2d3379c247364d65fc6ea66fe652210501436baa))
* **layout:** make params optional ([a177bda](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/a177bda944af70087c23f764b22f41ec11d8e01a))


### Features

* **auth:** add @auth0/nextjs-auth0 ([3c2d200](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3c2d2005532bc2c417d57f141643c9585cd3fd30))

# [1.5.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.4.0...v1.5.0) (2020-09-29)


### Bug Fixes

* remove env from now.json ([187f8ed](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/187f8edb66c97540320396ebc8ff17f88ebc14d2))
* update env vars ([fc59bba](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/fc59bbaaa528c6d6e3b9f092c7104fe399938e09))
* update readme ([d17dfe9](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/d17dfe9fb8f109fd976dd69a87c74595eb3d5108))
* **auth:** remove auth0-js dependency ([96efeb5](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/96efeb52df8539db0519598511c26fb29f43edee))
* **auth:** update env vars ([2d3379c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/2d3379c247364d65fc6ea66fe652210501436baa))
* **layout:** make params optional ([a177bda](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/a177bda944af70087c23f764b22f41ec11d8e01a))


### Features

* **auth:** add @auth0/nextjs-auth0 ([3c2d200](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3c2d2005532bc2c417d57f141643c9585cd3fd30))

# [1.4.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.3.0...v1.4.0) (2020-09-27)


### Bug Fixes

* **ci:** update lint job ([1df2086](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/1df208657367f7c2a54d07b627a4a81149afc510))
* **header:** toggle navbar on mobile ([a39f1f1](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/a39f1f1d133952f24f7ed03f33cc40fd43e67412))


### Features

* **footer:** add footer ([e7b2b05](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e7b2b0587932a8670576fb6edc1f56c4502bf67f))


### Performance Improvements

* **ci:** update gitignore ([76b3c0a](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/76b3c0ab39d2e5286ac0981263e3c7483db13740))

# [1.4.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.3.0...v1.4.0) (2020-09-27)


### Bug Fixes

* **ci:** update lint job ([1df2086](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/1df208657367f7c2a54d07b627a4a81149afc510))
* **header:** toggle navbar on mobile ([a39f1f1](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/a39f1f1d133952f24f7ed03f33cc40fd43e67412))


### Features

* **footer:** add footer ([e7b2b05](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e7b2b0587932a8670576fb6edc1f56c4502bf67f))


### Performance Improvements

* **ci:** update gitignore ([76b3c0a](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/76b3c0ab39d2e5286ac0981263e3c7483db13740))

# [1.3.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.2.0...v1.3.0) (2020-09-27)


### Features

* **header:** create header component ([249eb23](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/249eb23d06b387814e1451490dbee18f43a55a44))

# [1.3.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.2.0...v1.3.0) (2020-09-27)


### Features

* **header:** create header component ([249eb23](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/249eb23d06b387814e1451490dbee18f43a55a44))

# [1.2.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.1.0...v1.2.0) (2020-09-27)


### Bug Fixes

* **home:** update homepage to use layout ([aa62a8e](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/aa62a8e1a24c593853000485c64b37137a14be93))


### Features

* **seo:** update page layout ([92ae064](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/92ae064bbcbbdc0a12b59a4687997e1961f602bc))

# [1.2.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.1.0...v1.2.0) (2020-09-27)


### Bug Fixes

* **home:** update homepage to use layout ([aa62a8e](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/aa62a8e1a24c593853000485c64b37137a14be93))


### Features

* **seo:** update page layout ([92ae064](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/92ae064bbcbbdc0a12b59a4687997e1961f602bc))

# [1.1.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.0.0...v1.1.0) (2020-09-26)


### Bug Fixes

* **core:** add codeowners ([72aeb57](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/72aeb57fbd4ae2df0938c441071e1007789496e1))
* **core:** add lint-staged ([72c6d8c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/72c6d8ce4ddb96a9b0f417aecc10c07bd30def39))
* **test:** update test scripts ([3887136](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3887136cfbb644e85152bfa4537c16b17c447fb1))


### Features

* **core:** implement prettier eslint jest ([921f60c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/921f60cdf41ce1df8d3acae193f4e786660f9345))
* **core:** update to nextjs ([3f42ea3](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3f42ea3fb3d17dd983836bcc29a5c60f29bb9028))
* **page:** initialize pages ([0fa6075](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0fa60758973fb8775fc1572cbcf636044061478a))
* **style:** implement tailwind ([e428891](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e4288910ee66ff461288bee6d77809d6277345c2))
* **test:** add cypress testing ([3356684](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/33566847567b8a563b327ea5a4017920c3d2d294))


### Performance Improvements

* **ci:** remove codeowners ([e00b6ed](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e00b6ede570e2b71fddd532f2434fa3e389c6d4f))
* **test:** typo for setuptest ([7e831b0](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/7e831b056ca0e0f99c25124dfa6ae1a4b8e7c0ad))

# [1.1.0](https://gitlab.com/urbanlink/openpublicatie-frontend/compare/v1.0.0...v1.1.0) (2020-09-26)


### Bug Fixes

* **core:** add codeowners ([72aeb57](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/72aeb57fbd4ae2df0938c441071e1007789496e1))
* **core:** add lint-staged ([72c6d8c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/72c6d8ce4ddb96a9b0f417aecc10c07bd30def39))
* **test:** update test scripts ([3887136](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3887136cfbb644e85152bfa4537c16b17c447fb1))


### Features

* **core:** implement prettier eslint jest ([921f60c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/921f60cdf41ce1df8d3acae193f4e786660f9345))
* **core:** update to nextjs ([3f42ea3](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/3f42ea3fb3d17dd983836bcc29a5c60f29bb9028))
* **page:** initialize pages ([0fa6075](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0fa60758973fb8775fc1572cbcf636044061478a))
* **style:** implement tailwind ([e428891](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e4288910ee66ff461288bee6d77809d6277345c2))
* **test:** add cypress testing ([3356684](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/33566847567b8a563b327ea5a4017920c3d2d294))


### Performance Improvements

* **ci:** remove codeowners ([e00b6ed](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/e00b6ede570e2b71fddd532f2434fa3e389c6d4f))
* **test:** typo for setuptest ([7e831b0](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/7e831b056ca0e0f99c25124dfa6ae1a4b8e7c0ad))

# 1.0.0 (2020-09-26)


### Bug Fixes

* **ci:** create initial version ([5d727ec](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/5d727ec096f9d3a035ce17c6883cbab5a94d4214))
* **core:** update packages ([f70cc99](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/f70cc992ba295380696bf79c31c79ce52c9721d0))
* **core:** update releaserc ([405f057](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/405f0571613a37703d01e929d1ef1bbda5797623))


### Features

* **core:** implement semantic release ([7cae68c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/7cae68c53a36798fec1d88085cee073154de1ba7))


### Performance Improvements

* **ci:** add node-lts to install job ([6b2f494](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6b2f49478c69a26dfc1b5484a7c28934510cce18))
* **ci:** update semantic release config ([0c37df7](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0c37df7f0efab05ef0ab7fb67af8a100b2c569a1))

# 1.0.0 (2020-09-26)


### Bug Fixes

* **ci:** create initial version ([5d727ec](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/5d727ec096f9d3a035ce17c6883cbab5a94d4214))
* **core:** update packages ([f70cc99](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/f70cc992ba295380696bf79c31c79ce52c9721d0))
* **core:** update releaserc ([405f057](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/405f0571613a37703d01e929d1ef1bbda5797623))


### Features

* **core:** implement semantic release ([7cae68c](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/7cae68c53a36798fec1d88085cee073154de1ba7))


### Performance Improvements

* **ci:** add node-lts to install job ([6b2f494](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/6b2f49478c69a26dfc1b5484a7c28934510cce18))
* **ci:** update semantic release config ([0c37df7](https://gitlab.com/urbanlink/openpublicatie-frontend/commit/0c37df7f0efab05ef0ab7fb67af8a100b2c569a1))
