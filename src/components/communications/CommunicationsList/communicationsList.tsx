import React from 'react';
import { Communication } from 'src/types/Communication';
import { CommunicationsListItem } from '../CommunicationsListItem';

interface CommunicationsListProps {
  communications: Communication[];
}

export const CommunicationsList: React.FC<CommunicationsListProps> = ({
  communications,
}) => {
  return (
    <div>
      <h3>Communicatie</h3>
      {!communications && (
        <p>Er is nog geen communicatie over deze aanvraag.</p>
      )}
      {communications &&
        communications.map((item) => (
          <CommunicationsListItem communication={item} key={item.id} />
        ))}
    </div>
  );
};

export default CommunicationsList;
