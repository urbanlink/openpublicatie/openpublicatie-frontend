import React from 'react';
import dayjs from 'dayjs';
import { Communication } from 'src/types/Communication';

interface CommunicationsListItemProps {
  communication: Communication;
}
export const CommunicationsListItem: React.FC<CommunicationsListItemProps> = ({
  communication,
}) => {
  return (
    <div className="border border-primary bg-white mb-4">
      <div className="flex bg-primary text-white font-bold p-2">
        <div className="w-1/2">Van: {communication.from_user?.id}</div>
        <div className="w-1/2 text-right">
          {dayjs(communication.created_at).format('DD MMMM YYYY - HH:mm')}
        </div>
      </div>

      <div className="p-2 bg-gray-200">
        <strong>Onderwerp:</strong> {communication.subject}
      </div>
      <div className="p-2">
        <div dangerouslySetInnerHTML={{ __html: communication.body }}></div>
      </div>
    </div>
  );
};
