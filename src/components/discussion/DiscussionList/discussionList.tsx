import React from 'react';
import { Comment } from 'src/types/Comment';
import { DiscussionListItem } from '../DiscussionListItem';

interface DiscussionListProps {
  discussion: Comment[];
}

export const DiscussionList: React.FC<DiscussionListProps> = ({
  discussion,
}) => {
  if (!discussion) return null;

  return (
    <div>
      <h3>Discussie</h3>
      {discussion.length < 1 && (
        <p>Discussie op deze aanvaag is nog niet mogelijk.</p>
      )}
      {discussion &&
        discussion.map((item) => (
          <DiscussionListItem discussion={item} key={item.id} />
        ))}
    </div>
  );
};
