import React from 'react';
import { Comment } from 'src/types/Comment';

interface DiscussionListItemProps {
  discussion: Comment;
}

export const DiscussionListItem: React.FC<DiscussionListItemProps> = ({
  discussion,
}) => {
  return (
    <div>
      <div>{discussion.created}</div>
      <div>Van: {discussion.username}</div>
      <div>Onderwerp: {discussion.subject}</div>
      <div dangerouslySetInnerHTML={{ __html: discussion.body }}></div>
    </div>
  );
};
