import React from 'react';
import { File } from 'src/types/File';
import { FileListItem } from '../FilesListItem';

interface FilesListProps {
  files: File[];
}

export const FilesList: React.FC<FilesListProps> = ({ files }) => {
  return (
    <div>
      <h3>Bestanden</h3>
      {!files && (
        <p>Er zijn nog geen bestanden beschikbaar voor deze aanvraag.</p>
      )}
      {files && files.map((item) => <FileListItem file={item} key={item.id} />)}
    </div>
  );
};
