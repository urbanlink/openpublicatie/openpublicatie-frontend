import React from 'react';
import { File } from 'src/types/File';

interface FileListItemProps {
  file: File;
}
export const FileListItem: React.FC<FileListItemProps> = ({ file }) => {
  return <>File {file.id}</>;
};
