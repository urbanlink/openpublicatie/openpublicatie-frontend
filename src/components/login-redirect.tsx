import Router from 'next/router';
import React, { useEffect } from 'react';

import { Layout } from '../components/shared/Layout';
import createLoginUrl from '../utils/url-helper';

const RedirectToLogin: React.FC = () => {
  useEffect(() => {
    window.location.assign(createLoginUrl(Router.pathname));
  }, []);

  return (
    <Layout title="login" description="login" user={undefined} loading={false}>
      <div>Signing you in...</div>
    </Layout>
  );
};

export default RedirectToLogin;
