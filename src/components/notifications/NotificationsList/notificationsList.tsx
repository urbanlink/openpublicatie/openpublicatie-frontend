import React from 'react';
import { Notification } from 'src/types/Notification';
import { NotificationsListItem } from '../NotificationsListItem';

interface NotificationsListProps {
  notifications?: Notification[];
}

export const NotificationsList: React.FC<NotificationsListProps> = ({
  notifications,
}) => {
  return (
    <div>
      <h3>Notificaties</h3>
      {!notifications && <p>Nog geen notificaties.</p>}
      {notifications &&
        notifications.map((item) => (
          <NotificationsListItem notification={item} key={item.id} />
        ))}
    </div>
  );
};
