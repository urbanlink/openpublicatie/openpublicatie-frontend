import React from 'react';
import dayjs from 'dayjs';

import { Notification } from '../../../types/Notification';

interface NotificationsListItemProps {
  notification: Notification;
}

export const NotificationsListItem: React.FC<NotificationsListItemProps> = ({
  notification,
}) => {
  return (
    <div className="flex mb-1 pb-1 border-b-2 border-brown-light">
      <div className="w-1/4">
        {dayjs(notification.created_at).format('D MMMM YYYY')}
      </div>
      <div className="w-3/4">{notification.message}</div>
    </div>
  );
};
