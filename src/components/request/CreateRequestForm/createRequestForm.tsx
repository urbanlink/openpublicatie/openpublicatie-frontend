import React from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import Axios from 'axios';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as yup from 'yup';
import { useFetchUser } from '@utils/user';
import { useRouter } from 'next/router';

interface IFormInputs {
  title: string;
  description: string;
  organisation: string;
}

const schema = yup.object().shape({
  title: yup.string().required('Please enter a title'),
  description: yup.string().required('Please enter a description'),
  organisation: yup.string().required('Een organisatie is verplicht'),
});

export const CreateRequestForm: React.FC = () => {
  const { user, loading } = useFetchUser();
  const router = useRouter();

  const { register, handleSubmit, errors, setError, watch } = useForm<
    IFormInputs
  >({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const localValues = watch(['title', 'description', 'organisation']);

  const createRequest = async (data: IFormInputs) => {
    try {
      console.log('sending data', data);
      const r = await Axios.post('/api/request/create', { request: data });
      console.log('r', r.data);
      toast('Je aanvraag is aangemaakt en opgeslagen als concept. ');
      router.push(`/r/${r.data.slug}`);
    } catch (err) {
      console.log(err.response);
      if (err.response.data.code === 500) {
        console.log('There was an error at the server (500)');
        toast('There was an error at the server (500)');
      }
      if (err.response.data.code === 403) {
        toast.error(err.response.data.error.message);
      }
      if (err.response.data.code === 400) {
        // console.log(err.response.data.error);
        const e = err.response.data.error.message;
        if (e) {
          // console.log(e);
          e.map((error: any) => {
            console.log(error.message.join(', '));
            setError(error.field, { type: 'manua;', message: error.message });
            toast('Error creating event: ' + error.message.join(', '));
          });
        }
      }
    }
  };

  if (loading) return null;
  return (
    <div className="overflow-hidden px-3 py-10 mb-16 flex justify-center">
      <div className="w-2/3">
        <h2>Start een nieuw verzoek:</h2>
        <form
          className=" bg-background-light shadow-md rounded px-8 pt-6 pb-8 mb-4"
          onSubmit={handleSubmit(createRequest)}
        >
          <div className="mb-6">
            <label className="font-bold text-lg" htmlFor="organisation">
              Bij wie wil je de aanvraag doen?
            </label>
            <input
              className={`focus:outline-none focus:shadow-outline ${
                errors.description ? 'border-red-500' : ''
              }`}
              name="organisation"
              ref={register}
              autoFocus={true}
              type="text"
              placeholder="Bij wie wil je de data aanvraag indienen?"
            />
            <div className="text-sm mt-1">
              Bijvoorbeeld{' '}
              <em>&lsquo;Actueel bomenbestand van de gemeente&rsquo;</em> of
              <em>&lsquo;Subsidie-register 2020&rsquo;</em>. De ontvanger zit
              dit als onderwerp in het email-bericht.
            </div>
            {errors.organisation && (
              <p className="text-red-500 text-xs italic">
                {errors.organisation.message}
              </p>
            )}
          </div>

          <div className="mb-6">
            <label className="font-bold text-lg" htmlFor="title">
              Titel van de aanvraag
            </label>
            <input
              name="title"
              type="text"
              placeholder="Welke aanvraag wil je doen?"
              ref={register({ required: true, maxLength: 255 })}
              className={`${
                errors.title ? 'border-red-500 ' : ' '
              } focus:outline-none focus:shadow-outline`}
            />
            <div className="text-sm mt-1">
              Bijvoorbeeld{' '}
              <em>&lsquo;Actueel bomenbestand van de gemeente&rsquo;</em>{' '}
              of&nbsp;
              <em>&lsquo;Subsidie-register 2020&rsquo;</em>. De ontvanger zit
              dit als onderwerp in het email-bericht.
            </div>
            {errors && errors.title && (
              <p className="text-red-500 text-xs italic">
                {errors.title.message}
              </p>
            )}
          </div>

          <div className="mb-6">
            <label className="font-bold text-lg" htmlFor="description">
              Welke open data wil je aanvragen?
            </label>
            <textarea
              name="description"
              placeholder="Beschrijf je aanvraag zo compleet mogelijk"
              ref={register}
              rows={5}
              className={`focus:outline-none focus:shadow-outline ${
                errors.description ? 'border-red-500' : ''
              }`}
              style={{ whiteSpace: 'pre-wrap' }}
            />
            <div className="text-sm ">
              Beschrijf zo specifiek mogelijk de bestanden die je wilt ontvangen
              voor deze aanvraag.
            </div>
            {errors.description && (
              <p className="text-red-500 text-xs italic">
                {errors.description.message}
              </p>
            )}
          </div>

          <div className="mb-4 pt-8">
            Je aanvraag wordt eerst als concept opgeslagen in je account. Je
            kunt de aanvraag nog wijzigen voordat deze wordt ingediend.
          </div>
          <div className="flex items-center justify-between">
            <button
              className="bg-primary hover:bg-blue-700 w-full text-white py-2 px-4 rounded text-xl font-bold focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Aanvraag opslaan &rarr;
            </button>
          </div>
        </form>
      </div>
      <div className="w-1/3">
        <div className="ml-4">
          <h2>Preview</h2>
          <div className="bg-white shadow-md w-full p-4 rounded">
            <p className="mb-0">
              <strong>Van: </strong>
              {user.name}
            </p>
            <p className="">
              <strong>Aan: </strong>
              {localValues.organisation}
            </p>
            <p className="text-xl">
              <strong>Betreft: {localValues.title}</strong>
            </p>
            <div className="mt-4 ">
              <p>Geachte {localValues.organisation || '...'}, </p>

              <p>
                Hierbij ontvangt u het volgende verzoek voor het publiceren van
                open data:
              </p>
              {localValues.description &&
                localValues.description
                  .split('\n')
                  .map((str, i) => <p key={i}>{str}</p>)}

              <br />
              <p>
                Deze aanvraag is door mij aangemaakt en verstuurd via
                openpublicatie.nl. Ik verzoek u te antwoorden binnen de
                wettelijk gestelde termijn van 14 dagen.
              </p>
              <br />
              <p>Met vriendelijke groet, </p>
              <p>{user.name}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
