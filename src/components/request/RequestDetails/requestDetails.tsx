import dayjs from 'dayjs';
import React from 'react';
import { Request } from 'src/types/Request';
import { RequestStatus } from '../RequestStatus';

interface requestDetailsProps {
  request: Request;
}

export const RequestDetails: React.FC<requestDetailsProps> = ({ request }) => {
  return (
    <div className="bg-white p-2 md:p-6">
      <h3 className="font-bold mb-4">Aanvraag details</h3>
      <table className="mb-4">
        <tbody>
          <tr>
            <td className="font-bold pr-4 pb-2">Bronhouder</td>
            <td>{request.organization && request.organization.title}</td>
          </tr>
          <tr>
            <td className="font-bold pr-4 pb-2">Tracking #</td>
            <td>OPNL-{request.id}</td>
          </tr>
          <tr>
            <td className="font-bold pr-4 pb-2">Aangemaakt</td>
            <td>{dayjs(request.created_at).format('DD MMMM YYYY')}</td>
          </tr>
          {request.date_filed && (
            <tr>
              <td className="font-bold pr-4 pb-2">Ingediend</td>
              <td>{dayjs(request.date_filed).format('DD MMMM YYYY')}</td>
            </tr>
          )}
          {request.date_finished && (
            <tr>
              <td className="font-bold pr-4 pb-2">Afgerond</td>
              <td>{dayjs(request.date_finished).format('DD MMMM YYYY')}</td>
            </tr>
          )}
        </tbody>
      </table>

      <div className="py-4 text-center border-t">
        <RequestStatus status={request.status} />
      </div>

      <div className="py-4 text-center border-t">
        Aantal dagen sinds indiening:
        <div className="text-3xl font-bold">101</div>
      </div>
    </div>
  );
};
