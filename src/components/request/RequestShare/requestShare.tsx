import React from 'react';
import {
  FaFacebook,
  FaLinkedin,
  FaMailBulk,
  FaReddit,
  FaTwitter,
  FaWhatsapp,
} from 'react-icons/fa';
import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  RedditShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from 'react-share';
import { Request } from 'src/types/Request';

interface RequestShareProps {
  request: Request;
}

export const RequestShare: React.FC<RequestShareProps> = ({ request }) => {
  const shareUrl = `https://openpublicatie.nl/r/${request.slug}`;
  const quote = `${request.title} via @openpublicatie`;

  return (
    <div className="text-center mt-8 mb-4">
      <TwitterShareButton url={shareUrl} title={quote} className="mr-2">
        <FaTwitter size={18} />
      </TwitterShareButton>
      <LinkedinShareButton url={shareUrl} title={quote} className="mr-2">
        <FaLinkedin size={18} />
      </LinkedinShareButton>
      <FacebookShareButton url={shareUrl} title={quote} className="mr-2">
        <FaFacebook size={18} />
      </FacebookShareButton>
      <RedditShareButton url={shareUrl} title={quote} className="mr-2">
        <FaReddit size={18} />
      </RedditShareButton>
      <WhatsappShareButton url={shareUrl} title={quote} className="mr-2">
        <FaWhatsapp size={18} />
      </WhatsappShareButton>
      <EmailShareButton url={shareUrl} title={quote} className="mr-2">
        <FaMailBulk size={18} />
      </EmailShareButton>
    </div>
  );
};
