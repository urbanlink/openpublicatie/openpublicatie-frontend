import React from 'react';

const v = {
  concept: {
    value: 'concept',
    state: 'neutral',
    class: 'bg-gray-300 text-gray-600',
  },
  ready_for_sending: {
    value: 'Klaar voor verzending',
    state: 'neutral',
    class: 'bg-gray-400 text-black',
  },
  awaiting_confirmation: {
    value: 'Wachtend op bevestiging',
    state: 'warning',

    class: 'bg-orange-400 text-white',
  },
  awaiting_answer: {
    value: 'Wachtend op antwoord',
    state: 'warning',
    class: 'bg-orange-400 text-white',
  },
  improvement_needed: {
    value: 'Verbetering vereist',
    state: 'warning',
    class: 'bg-orange-400 text-white',
  },
  declined: {
    value: 'Afgewezen',
    state: 'warning',
    class: 'bg-orange-400 text-white',
  },
  incomplete: {
    value: 'Incompleet',
    state: 'warning',
    class: 'bg-orange-400 text-white',
  },
  finished: {
    value: 'Agerond',
    state: 'success',
    class: 'bg-green-400 text-white',
  },
  abandoned: {
    value: 'Ingetrokken',
    state: 'danger',
    class: 'bg-red-400 text-white',
  },
};

interface RequestStatusProps {
  status: string;
}

export const RequestStatus: React.FC<RequestStatusProps> = ({ status }) => {
  return (
    <span className={`${v[status].class} px-2 py-1 rounded`}>
      {v[status].value}
    </span>
  );
};
