import Link from 'next/link';
import React from 'react';

interface ButtonProps {
  href: string;
  variant?: 'primary' | 'secondary';
}

export const Button: React.FC<ButtonProps> = ({ children, href }) => {
  return (
    <Link href={href}>
      <a
        href={href}
        className="transition-colors duration-300 font-header tracking-wider px-4 py-2 no-underline inline-block bg-primary text-white uppercase rounded hover:bg-secondary hover:text-white"
      >
        {children}
      </a>
    </Link>
  );
};
