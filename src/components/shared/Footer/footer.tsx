import { NextPage } from 'next';
import Link from 'next/link';
import { FaFileDownload } from 'react-icons/fa';

export const Footer: NextPage = () => {
  return (
    <footer className="py-16 bg-background-light">
      <div className="container">
        <div className="text-sm leading-loose sm:flex mb-0">
          <div className="sm:w-1/2 h-auto pr-4">
            <p>
              <FaFileDownload />
              Open Publicatie geeft jou de mogelijkheden om onze samenleving
              meer transparant te maken.
            </p>
          </div>
          <div className="sm:w-1/2 h-auto sm:mt-0 mt-8 px-4 text-right">
            <ul className="list-reset leading-normal">
              <li className="text-grey-darker">
                <Link href="/about">
                  <a className="hover:underline">About</a>
                </Link>
              </li>
              <li className="text-grey-darker">
                <Link href="/contact">
                  <a className="hover:underline">Contact</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="pt-8 mt-8 text-sm text-brown-light border-t border-brown-light">
          <p>
            2016-2020 openpublicatie.nl, door{' '}
            <a href="https://urbanlink.nl">urbanlink.nl</a>
          </p>
        </div>
      </div>
    </footer>
  );
};
