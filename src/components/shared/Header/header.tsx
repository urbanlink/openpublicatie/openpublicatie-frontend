import { NextPage } from 'next';
import Link from 'next/link';
import { useState } from 'react';
import { FaFileDownload } from 'react-icons/fa';

import { useUser } from '../../../utils/user';

export const Header: NextPage = () => {
  const { user, loading } = useUser();
  const [open, setOpen] = useState(false);

  const navItems = [
    {
      title: 'Verzoeken',
      href: '/verzoeken',
    },
    {
      title: 'Hoe werkt het?',
      href: '/hoe-werkt-het',
    },
    {
      title: 'Voor wie',
      href: '/voor-wie',
    },
  ];
  return (
    <nav className="font-heading flex items-center justify-between flex-wrap p-6">
      <div className="flex items-center flex-no-shrink mr-6">
        <Link href="/">
          <a className="font-light tracking-wider text-xl font-header no-underline">
            <FaFileDownload className="inline mr-2" />
            Open Publicatie
          </a>
        </Link>
      </div>
      <div className="block lg:hidden">
        <button
          onClick={() => setOpen(!open)}
          className="flex items-center px-3 py-2 borderrounded text-grey border-teal-light hover:text-black hover:border-black"
        >
          <svg
            className="h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      <div
        className={`w-full bg-background z-20 items-center lg:flex lg:w-auto ${
          open ? ' block' : ' hidden'
        }`}
      >
        <div className="text-sm">
          {navItems.map((item, index) => (
            <Link key={index} href={item.href}>
              <a className="block mt-4 leading-loose font-header no-underline lg:inline-block lg:mt-0 mr-4">
                {item.title}
              </a>
            </Link>
          ))}
          {!loading &&
            (user ? (
              <>
                <Link href="/profile">
                  <a className="block font-header no-underline mt-4 leading-loose lg:inline-block lg:mt-0 hover:underline mr-4">
                    Profiel
                  </a>
                </Link>
                <Link href="/api/logout">
                  <a className="block font-header no-underline mt-4 leading-loose lg:inline-block lg:mt-0 hover:underline mr-4">
                    Uitloggen
                  </a>
                </Link>
              </>
            ) : (
              <>
                <Link href="/api/login">
                  <a className="block font-header no-underline mt-4 leading-loose lg:inline-block lg:mt-0 hover:underline mr-4">
                    Inloggen
                  </a>
                </Link>
              </>
            ))}
        </div>

        <a
          href="/nieuw"
          className="transition-colors duration-300 inline-block bg-primary text-white font-header no-underline text-sm px-4 py-4 leading-none rounded-full hover:text-white hover:bg-secondary mt-4 lg:mt-0"
        >
          Nieuw verzoek indienen
        </a>
      </div>
    </nav>
  );
};
