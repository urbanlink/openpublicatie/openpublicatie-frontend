import Head from 'next/head';
import React from 'react';
import { Footer } from './../Footer';
import { Header } from './../Header';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { UserProvider } from './../../../utils/user';

interface LayoutProps {
  title?: string;
  description?: string;
  user?: any;
  loading?: boolean;
  children: React.ReactNode;
}
export const Layout: React.FC<LayoutProps> = ({
  children,
  title,
  description,
  user,
  loading,
}: LayoutProps) => {
  return (
    <UserProvider value={{ user, loading }}>
      <div className="flex flex-col h-screen w-full">
        <Head>
          <title>{title} | Open Publicatie</title>
          <link rel="icon" href="/favicon.svg" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <meta name="description" content={description} />
          <meta name="og:title" content={title} />
          <meta name="og:description" content={description} />
          <meta name="og:type" content="website" />
          <meta name="twitter:card" content={description} />
          <meta name="twitter:creator" content="openpublicatie" />
          <meta name="twitter:title" content={title} />
          <meta name="twitter:description" content={description} />
        </Head>
        <Header />
        {children}
        <Footer />
        <ToastContainer />
      </div>
    </UserProvider>
  );
};
