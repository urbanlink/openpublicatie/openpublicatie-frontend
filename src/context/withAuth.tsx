/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React, { Component } from 'react';

import auth0 from '../utils/auth0';
import { fetchUser } from '../utils/user';
import createLoginUrl from '../utils/url-helper';
import RedirectToLogin from '../components/login-redirect';

interface AuthProps {
  user: any;
}

export default function withAuth(InnerComponent: any): any {
  return class Authenticated extends Component {
    static async getInitialProps(ctx) {
      if (!ctx.req) {
        const user = await fetchUser();
        return {
          user,
        };
      }

      const session = await auth0.getSession(ctx.req);
      if (!session || !session.user) {
        ctx.res.writeHead(302, {
          Location: createLoginUrl(ctx.req.url),
        });
        ctx.res.end();
        return;
      }

      return { user: session.user };
    }

    constructor(props: AuthProps) {
      super(props);
    }

    render() {
      const { user } = this.props as any;
      if (!user) {
        return <RedirectToLogin />;
      }

      return <div>{<InnerComponent {...this.props} user={user} />}</div>;
    }
  };
}
