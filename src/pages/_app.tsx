import * as React from 'react';
import { useRouter } from 'next/router';
import type { AppProps } from 'next/app';
import NProgress from 'nprogress';
import 'fontsource-alegreya';
import 'fontsource-alegreya/700.css';
import 'fontsource-roboto-slab';
import 'fontsource-roboto-slab/300.css';
import 'fontsource-roboto-slab/600.css';
import 'fontsource-roboto-slab/700.css';
import * as gtag from '../utils/gtag';

import '@styles/main.css';
import '@styles/nprogress.css';

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  const router = useRouter();
  React.useEffect(() => {
    const handleRouteChange = (url: URL) => {
      NProgress.done();
      gtag.pageview(url);
    };
    router.events.on('routeChangeStart', () => NProgress.start());
    router.events.on('routeChangeComplete', handleRouteChange);
    router.events.on('routeChangeError', () => NProgress.done());

    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
      router.events.off('routeChangeError', () => NProgress.done());
    };
  }, [router.events]);
  return <Component {...pageProps} />;
};

export default App;
