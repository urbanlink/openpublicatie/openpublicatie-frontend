import { NextPage } from 'next';
import React from 'react';
import { Layout } from '../components/shared/Layout';

const AboutPage: NextPage = () => {
  return (
    <Layout title="About" description="Over Open Publicatie">
      <div className="container">
        <section className="text-center py-8">
          <h1>Over Open Publicatie</h1>
        </section>

        <section className="pb-16">
          <p>
            Open Publicatie is een project van{' '}
            <a href="https://urbanlink.nl">urbanlink.nl</a>. Het is een
            ondersteunend platform die journalisten, onderzoekers, activisten,
            belangenorganisaties en bewoners de mogelijkheid biedt om documenten
            op te vragen, te analyseren en te delen. Dit maakt de overheid en
            samenleving meer transparant en de democratie sterker en beter
            geïnformeerd.
          </p>

          <p>
            Deze site biedt een bibliotheek van alle documenten en data van
            overheid en organisaties, informatie over hoe je een open publicatie
            verzoek indiend, en hulpmiddelen om het verzoek-proces eenvoudiger
            te maken. Daarnaast maakt het netwerk van mensen rondom Open
            Publicatie gebruik van de documentenen tools om onderzoek en analyse
            uit te voeren.
          </p>

          <p>
            Open Publicatie is ontstaan vanuit het idee om WOB verzoeken meer
            inzichtelijk te maken. Uit eigen ervaring bleek dat dit proces nog
            veel onduidelijkheden opriep een een sfeer van geheimzinnigheid had.
            En dat terwijl het een relatief eenvoudig proces is dat is gericht
            op transparantie en accountability. <br />
            Dit project is gestart onder de naam <em>nonstopwobshop</em>, en
            daarna doorgegroeid naar <strong>Open Publicatie</strong>.
          </p>

          <p>
            De gegevens die op Open Publicatie staan zijn kostenloos
            beschikbaar. Voor het indienen van verzoeken en het reageren op
            verzoeken is een account nodig op Open Publicatie. Het indienen van
            aanvragen zal altijd kostenloos blijven, maar wel met een maximum
            aantal aanvragen per maand. Op een gegeven moment zullen er pro
            accounts zijn waarmee je meer aanvragen kan doen of andere extra
            functies kan gebruiken.
          </p>
        </section>
      </div>
    </Layout>
  );
};

export default AboutPage;
