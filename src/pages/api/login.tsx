/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import auth0 from '../../utils/auth0';

export default async function login(req: any, res: any): Promise<any> {
  try {
    await auth0.handleLogin(req, res, {
      authParams: {
        ui_locales: 'nl',
        // scope: 'token profile email',
      },
      redirectTo: '/dashboard',
    });
  } catch (error) {
    console.error(error);
    res.status(error.status || 400).end(error.message);
  }
}
