/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import auth0 from '../../utils/auth0';

export default async function logout(req: any, res: any): Promise<any> {
  try {
    await auth0.handleLogout(req, res);
  } catch (error) {
    console.error(error);
    res.status(error.status || 400).end(error.message);
  }
}
