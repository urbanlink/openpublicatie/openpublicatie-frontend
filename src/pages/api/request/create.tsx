import Axios from 'axios';
import { NextApiRequest, NextApiResponse } from 'next';
import auth0 from '../../../utils/auth0';

export default async function createRequest(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> {
  try {
    const tokenCache = auth0.tokenCache(req, res);
    const { accessToken } = await tokenCache.getAccessToken();

    const url = process.env.NEXT_PUBLIC_API_URL + 'request';

    const response = await Axios.post(url, req.body, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },
    });

    console.log('response', response.data);
    return res.status(200).json(response.data);
  } catch (error) {
    // console.error('err er e', error.response.data);
    console.log(error);
    res.status(error.response.data.statusCode || 500).json({
      code: error.response.data.statusCode,
      error: error.response.data,
    });
  }
}
