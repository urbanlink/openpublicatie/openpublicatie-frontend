import { NextApiResponse } from 'next';
import Axios from 'axios';

export default async function fetchRequests(
  res: NextApiResponse
): Promise<any> {
  try {
    const url = `${process.env.NEXT_PUBLIC_API_URL}request}`;

    const response = await Axios.get(url, {
      headers: {
        'Content-Type': 'application/json',
      },
    });

    // console.log('response', response.data);
    return res.status(response.data.statusCode).json(response.data);
  } catch (error) {
    // console.log(error);
    res.status(error.response.data.statusCode || 500).json({
      code: error.response.data.statusCode,
      error: error.response.data,
    });
  }
}
