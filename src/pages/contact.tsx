import { NextPage } from 'next';
import { Layout } from '../components/shared/Layout';

const ContactPage: NextPage = () => {
  return (
    <Layout title="Contact" description="Contact">
      <div className="container">
        <section className="text-center py-16">
          <h1 className="text-4xl">Contact</h1>
        </section>

        <section className="pb-16">
          <p>
            Open publicatie is een project van{' '}
            <a href="https://urbanlink.nl">urbanlink.nl</a>. Neem contact op per
            mail: <a href="mailto:arn@urbanlink.nl">arn@urbanlink.nl</a>
          </p>
        </section>
      </div>
    </Layout>
  );
};

export default ContactPage;
