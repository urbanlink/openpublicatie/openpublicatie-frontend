import { NextPage } from 'next';
import React from 'react';
import withAuth from 'src/context/withAuth';
import { Layout } from '../components/shared/Layout';

const AboutPage: NextPage = () => {
  return (
    <Layout title="Dashboard" description="Jouw dashboard">
      Dashboard
    </Layout>
  );
};

export default withAuth(AboutPage);
