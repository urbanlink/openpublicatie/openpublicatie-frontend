import { Button } from '@components/shared/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import { Layout } from '../components/shared/Layout';
import { useFetchUser } from '../utils/user';
import { faCheckSquare, faDownload } from '@fortawesome/free-solid-svg-icons';
import { faGitlab, faTelegram } from '@fortawesome/free-brands-svg-icons';

const Home: NextPage = () => {
  const { user, loading } = useFetchUser();

  return (
    <Layout
      user={user}
      loading={loading}
      title="Home"
      description="Open publicatie"
    >
      <main className="main">
        <section className="py-16">
          <div className="container relative text-center">
            <h1 className="text-3xl mb-8 font-light md:text-6xl">
              Open Publicatie
            </h1>
            <h2 className="text-lg max-w-xl m-auto mb-8 font-light md:text-3xl">
              Maak, volg en deel verzoeken voor Open Data.
            </h2>
            {!user && !loading && (
              <Button href="/api/login">
                <FontAwesomeIcon
                  icon={faDownload}
                  size="lg"
                  color="white"
                  style={{ display: 'inline' }}
                />
                &nbsp;&nbsp;Meld je aan en dien een verzoek in
              </Button>
            )}
            {user && (
              <Button href="/nieuw">
                Hoi{' ' + user.nickname}, maak een nieuw verzoek aan &rarr;
              </Button>
            )}
          </div>
        </section>

        <section className="py-16">
          <div className="container">
            <div className="md:grid md:grid-cols-2 md:gap-4 pb-8 mb-8 border-b-2 border-primary">
              <div className="column">
                <h3 className="text-xl mb-2 font-bold">
                  Maak automatisch open data verzoeken.
                </h3>
                <p className="text-xl mb-8 flex-1">
                  Onze eenvoudige tool maakt het voor iedereen mogelijk om
                  verzoeken voor open data in te dienen.
                </p>
              </div>
              <div className="column">
                <ul className="fa-ul">
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    Eenvoudige aanvraag wizard
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    Genereer een eigen aanvraag in een paar makkeijke stappen
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Selecteer de relevante organisatie uit onze database{' '}
                    <em>(in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Kies de voorwaarden die passen bij de aanvraag
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Maak, wijzig en deel nieuwe contacten en data met de Open
                    Publicatie community<em> (in ontwikkeling)</em>
                  </li>
                </ul>
              </div>
            </div>
            <div className="md:grid md:grid-cols-2 md:gap-4 pb-8 mb-8 border-b-2 border-primary">
              <div className="column">
                <h3 className="text-xl mb-2 font-bold">
                  Dien de aanvraag in en volg de status.
                </h3>
                <p className="text-xl mb-8">
                  Gebruik jouw Open Publicatie account om de voortgang van al je
                  aanvragen te volgen. Vanuit 1 locatie.
                </p>
              </div>
              <div className="column">
                <ul className="fa-ul">
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    De aanvraag wordt automatisch gegenereerd en verstuurd per
                    email naar jouw geselecteerde organisatie
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Voeg bijlagen en een persoonlijke tekst toe
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Sla alle offline corresponentie op
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    De ontvangen antwoorden worden toegevoegd aan je project{' '}
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Dien automatisch beroep aan<em> (in ontwikkeling)</em>
                  </li>
                </ul>
              </div>
            </div>

            <div className="md:grid md:grid-cols-2 md:gap-4 pb-8 mb-8 border-b-2 border-primary">
              <div className="column">
                <h3 className="text-xl mb-2 font-bold">
                  Powered by the community
                </h3>
                <p className="text-xl mb-4">
                  Open Publicatie is open source en onafhankelijk. Ontwikkeling
                  vindt door de community plaats.
                </p>
              </div>
              <div className="column">
                <ul className="fa-ul">
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    Volg alle communicatie met de organisatie
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    Deel projecten met je collega&apos;s
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      className="text-green-600"
                    />
                    Beheer alle verstuurde en ontvangen documenten
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Beheer meerdere aanvragen in je project
                    <em> (in ontwikkeling)</em>
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheckSquare}
                      listItem
                      color="grey"
                    />
                    Set reminders and follow-ups for any submitted requests
                    <em> (in ontwikkeling)</em>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>

        <section className="py-16 text-center bg-primary text-white">
          <div className="container">
            <h3 className="mb-4">Neem deel aan onze community kanalen!</h3>
            <div className="text-center pt-4">
              <span className="inline-block mx-4 text-center">
                <a
                  href="https:/t.me/openpublicatie"
                  target="_blank"
                  rel="noreferrer"
                  className="no-underline"
                >
                  <FontAwesomeIcon
                    icon={faTelegram}
                    className="block m-auto mb-2 text-6xl"
                  />
                  Telegram
                </a>
              </span>
              <span className="inline-block mx-4 text-center">
                <a
                  href="https://gitlab.com/urbanlink/openpublicatie-frontend"
                  target="_blank"
                  rel="noreferrer"
                  className="no-underline"
                >
                  <FontAwesomeIcon
                    icon={faGitlab}
                    className="block m-auto mb-2 text-6xl"
                  />
                  Gitlab
                </a>
              </span>
            </div>
          </div>
        </section>

        <section className="py-16 text-center bg-white">
          <div className="container">
            <h3 className="mb-4">
              Wacht niet langer, start vandaag je aanvraag!
            </h3>
            {!user && <Button href="/api/login">Meld je gratis aan</Button>}
            {user && <Button href="/nieuw">Dien een nieuw verzoek in</Button>}
          </div>
        </section>
      </main>
    </Layout>
  );
};

export default Home;
