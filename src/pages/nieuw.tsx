import { CreateRequestForm } from '@components/request/CreateRequestForm/createRequestForm';
import auth0 from '@utils/auth0';
import { useFetchUser } from '@utils/user';
import { GetServerSideProps, NextPage } from 'next';
import { Layout } from '../components/shared/Layout';

const NewRequestPage: NextPage = () => {
  const { user, loading } = useFetchUser();

  return (
    <Layout
      user={user}
      loading={loading}
      title="Nieuw verzoek"
      description="Dien een nieuw verzoek in"
    >
      <div className="container">
        <section className="text-center py-16">
          <h1 className="text-4xl">Nieuw verzoek aanmaken</h1>
        </section>

        <section className="pb-16">
          <CreateRequestForm />
        </section>
      </div>
    </Layout>
  );
};

// Get data on SSR (authenticated page view and jwt for POST request to API)
export const getServerSideProps: GetServerSideProps = async ({
  req,
  res,
}): Promise<any> => {
  // Get idToken from auth0 session
  const requestConfig = await auth0.getSession(req);

  // Redirect to login when not authenticated
  if (!requestConfig || !requestConfig.user) {
    console.log('not authenticated, redirect to login');
    res.setHeader('location', '/api/login');
    res.statusCode = 302;
    res.end();
    return;
  }

  return { props: { requestConfig } };
};

export default NewRequestPage;
