import Link from 'next/link';
import React from 'react';
import withAuth from 'src/context/withAuth';

import { Layout } from '../components/shared/Layout';
import { useFetchUser } from '../utils/user';

const ProfilePage = () => {
  const { user, loading } = useFetchUser();

  return (
    <Layout
      user={user}
      loading={loading}
      title="Profiel"
      description="Gebruikersprofiel"
    >
      <header className="text-center py-8">
        <h1>Profiel</h1>
      </header>

      <section className="pb-16">
        <div className="container">
          {loading && <p>Loading profile...</p>}

          {!loading && user && (
            <div className="m-auto max-w-md">
              <table className="table">
                <tbody>
                  <tr>
                    <td>
                      <img
                        src={user.picture}
                        height="30px"
                        width="30px"
                        className="rounded-full my-8"
                      />
                    </td>
                    <td>{user.nickname}</td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Email: </strong>
                    </td>
                    <td> {user.name}</td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Sub: </strong>
                    </td>
                    <td>{user.sub}</td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Updated at: </strong>
                    </td>
                    <td>{user.updated_at}</td>
                  </tr>
                </tbody>
              </table>

              <Link href="/api/logout">
                <a className="inline-block rounded px-2 bg-primary font-header no-underline mt-4 leading-loose lg:inline-block lg:mt-0 text-teal-lighter hover:text-black hover:underline mr-4">
                  Uitloggen
                </a>
              </Link>
            </div>
          )}
        </div>
      </section>
    </Layout>
  );
};

export default withAuth(ProfilePage);
