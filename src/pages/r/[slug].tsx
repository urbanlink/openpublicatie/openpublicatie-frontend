import { CommunicationsList } from '@components/communications/CommunicationsList';
import { DiscussionList } from '@components/discussion/DiscussionList';
import { FilesList } from '@components/files/FilesList';
import { NotificationsList } from '@components/notifications/NotificationsList';
import { RequestDetails } from '@components/request/RequestDetails';
import { RequestShare } from '@components/request/RequestShare';
import { RequestStatus } from '@components/request/RequestStatus';
import { Layout } from '@components/shared/Layout';
import Axios, { AxiosResponse } from 'axios';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import ErrorPage from 'next/error';
import Link from 'next/link';
import { useState } from 'react';
import { Request } from 'src/types/Request';

const RequestPage: NextPage<{ request: Request }> = ({ request }) => {
  console.log(request);
  if (!request?.slug) {
    return <ErrorPage statusCode={404} />;
  }
  const { title, status } = request;

  enum Tabs {
    overview,
    communications,
    comments,
    publications,
    notifications,
  }
  const [activeTab, setActiveTab] = useState<Tabs>(Tabs.overview);

  const tabsMenu = [
    { tab: Tabs.overview, title: 'Overzicht' },
    { tab: Tabs.communications, title: 'Communicatie', count: true },
    { tab: Tabs.comments, title: 'Discussie' },
    { tab: Tabs.publications, title: 'Publicaties', count: true },
    { tab: Tabs.notifications, title: 'Notificaties' },
  ];

  return (
    <>
      <Layout title={title}>
        <div className="container">
          <header className="text-center pt-8 pb-24">
            <h2 className="font-sans font-thin uppercase">
              Verzoek tot open publicatie
            </h2>
            <h1>{title}</h1>
            <RequestStatus status={status} />
            <br />
            <div>
              {/* <span className="font-bold">{user.email}</span> heeft dit verzoek
              ingediend bij <span className="font-bold">{organisation}</span>. */}
            </div>
          </header>
          <main className="md:flex">
            <aside className="sidebar-left w-full md:w-1/3">
              <RequestDetails request={request} />
              <RequestShare request={request} />
              <div className="mt-4 mb-8 border-primary border-solid border bg-white p-4 text-center text-sm rounded">
                <p className="m-0">
                  Gebruikers van Open Publicatie kunnen verzoeken voor open data
                  aanmaken, indienen en volgen.{' '}
                  <Link href="/nieuw">
                    Maak ook een verzoek aan zoals het verzoek op deze pagina
                  </Link>
                  .
                </p>
              </div>
            </aside>

            <div className="w-full md:flex-grow md:px-4 mb-24">
              <div className="mb-4">
                <nav className="border-b-2 border-primary">
                  {tabsMenu.map((tab: any) => (
                    <button
                      key={tab.title}
                      onClick={() => setActiveTab(tab.tab)}
                      className={`font-header text-gray-600 inline-block px-2 py-1 md:py-4 md:px-6 hover:text-primary focus:outline-none ${
                        activeTab === tab.tab
                          ? 'text-primary font-medium border-primary bg-white'
                          : ''
                      }`}
                    >
                      {tab.title}
                      {tab.count && request[Tabs[tab.tab]] && (
                        <span className="bg-primary text-xs py-1 px-2 rounded ml-2 text-white">
                          {request[Tabs[tab.tab]].length}
                        </span>
                      )}
                    </button>
                  ))}
                </nav>
              </div>

              <div className="md:p-2">
                {activeTab === Tabs.overview && (
                  <>
                    <h3 className="font-bold">
                      Algemene informatie over deze aanvraag
                    </h3>
                    {request.description}
                  </>
                )}
                {activeTab === Tabs.communications && (
                  <CommunicationsList communications={request.communications} />
                )}
                {activeTab === Tabs.comments && (
                  <DiscussionList discussion={request.comments} />
                )}
                {activeTab === Tabs.publications && (
                  <FilesList files={request.files} />
                )}
                {activeTab === Tabs.notifications && (
                  <NotificationsList notifications={request.notifications} />
                )}
              </div>
            </div>
          </main>
        </div>
      </Layout>
    </>
  );
};

export default RequestPage;

// getServerSideProps (Server-side Rendering): Fetch data on each request.
// export const getServerSideProps: GetServerSideProps = () => {
//   return {};
// };

//  (Static Generation): Fetch data at build time.
export const getStaticProps: GetStaticProps = async ({
  params,
}): Promise<{ props: { request: Request } }> => {
  const request = await Axios.get(
    process.env.NEXT_PUBLIC_API_URL + 'request/' + params.slug
  );
  const r: Request = request.data;
  return {
    props: {
      request: r,
    },
  };
};

//  (Static Generation): Specify dynamic routes to pre-render based on data.
export const getStaticPaths: GetStaticPaths = async () => {
  // console.log('getStaticPaths: Fetch requests');
  // console.log('api url', process.env.NEXT_PUBLIC_API_URL);
  const response: AxiosResponse = await Axios.get(
    process.env.NEXT_PUBLIC_API_URL + 'request'
  );
  const requests: Request[] = response.data;
  // console.log('getStaticPaths: Number of requests: ' + requests.length);
  return {
    paths: requests.map((request: Request) => `/r/${request.slug}`),
    fallback: true,
  };
};
