import { NextPage, GetStaticProps, InferGetStaticPropsType } from 'next';
import Link from 'next/link';
import React from 'react';
import { Request } from 'src/types/Request';
import { Layout } from '../components/shared/Layout';

const RequestsPage: NextPage = ({
  requests,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  console.log(requests);

  return (
    <Layout
      title="Verzoeken"
      description="Overzicht van de verzoeken tot open publicatie "
    >
      <header className="text-center py-8">
        <div className="container">
          <h1 className="font-bold">Verzoeken</h1>
          <h2 className="max-w-lg m-auto font-light">
            We beheren alle verzoeken die via Open Publicatie zijn ingediend,
            zodat deze publiek en inzichtelijk worden.
          </h2>
        </div>
      </header>

      <section className="py-8">
        <div className="container">
          {!requests ||
            (requests.length === 0 && (
              <p>Er zijn nog geen verzoeken ingediend. </p>
            ))}
          {requests && (
            <div className="flex flex-wrap overflow-hidden min-w-xl">
              {requests.map((request: Request) => (
                <div key={request.title} className="w-ful md:w-1/2 lg:w-1/4">
                  <div className="overflow-hidden rounded-sm shadow-lg bg-white">
                    <Link href={`r/${request.slug}`}>
                      <a className="block no-underline hover:underline text-black p-4 hover:bg-gray-200">
                        <h1 className="text-lg font-bold">{request.title}</h1>
                      </a>
                    </Link>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </section>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'request');
  const requests: Request[] = await res.json();
  return {
    props: {
      requests,
    },
  };
};

export default RequestsPage;
