import { NextPage } from 'next';
import { Layout } from '../components/shared/Layout';

const ForWhoPage: NextPage = () => {
  return (
    <Layout title="Voor wie" description="Voor wie is Open Publicatie">
      <section className="text-3xl text-center py-8">
        <div className="container">
          <h1>Voor wie is Open Publicatie</h1>
          <h2 className="text-xl">
            Open Publicatie is van meerwaarde voor zowel de aanvrager als de
            bronhouder
          </h2>
        </div>
      </section>

      <section className="py-8">
        <div className="container">
          <div className="pb-16">
            <h3>(Onderzoeks) journalisten</h3>
            <ul className="list-disc">
              <li>Beheer je verzoeken op 1 locatie</li>
              <li>Krijg inzicht in de status van de aanvraag</li>
              <li>Integreer de aanvragen in je eigen website </li>
              <li>Gebruik de website om de gegevens te publiceren</li>
            </ul>

            <h3>Bewoners</h3>
            <ul className="list-disc">
              <li>
                Krijg ondersteuning in het proces van aanvragen voor open
                publicatie
              </li>
              <li>
                Kom in contact met de juiste contactpersoon voor je aanvraag
              </li>
              <li>Krijg inzicht in de status van de aanvraag</li>
              <li>Integreer de aanvragen in je eigen website </li>
              <li>Gebruik de website om de gegevens te publiceren</li>
            </ul>

            <h3>Belangenorganisaties</h3>
            <ul className="list-disc">
              <li>
                Krijg ondersteuning in het proces van aanvragen voor open
                publicatie
              </li>
              <li>
                Kom in contact met de juiste contactpersoon voor je aanvraag
              </li>
              <li>Beheer je verzoeken op 1 locatie</li>
              <li>Krijg inzicht in de status van de aanvraag</li>
              <li>Integreer de aanvragen in je eigen website </li>
              <li>Gebruik de website om de gegevens te publiceren</li>
            </ul>

            <h3>Bronhouders</h3>
            <ul className="list-disc">
              <li>Beheer aanvragen via 1 portaal</li>
              <li>Krijg direct inzicht in de status van alle aanvragen</li>
              <li>Integreer de aanvragen in je eigen website</li>
            </ul>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default ForWhoPage;
