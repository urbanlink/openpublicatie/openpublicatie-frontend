export interface Comment {
  id: number;
  title: string;
  created: string;
  username: string;
  body: string;
  subject: string;
}
