import { User } from './User';

export interface Communication {
  id: number;
  created_at: Date;
  updated_at: Date;
  subject: string;
  body: string;
  to_user: User;
  from_user: User;
}
