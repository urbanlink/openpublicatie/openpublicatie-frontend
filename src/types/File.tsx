export interface File {
  id: number;
  title: string;
}
