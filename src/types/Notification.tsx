export interface Notification {
  id: number;
  message: string;
  created_at: Date;
  updated_at: Date;
}
