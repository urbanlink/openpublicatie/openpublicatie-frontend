export interface Organisation {
  id: number;
  title: string;
}
