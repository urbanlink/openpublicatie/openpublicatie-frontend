import { Comment } from './Comment';
import { Communication } from './Communication';
import { File } from './File';
import { Notification } from './Notification';
import { Organisation } from './Organisation';
import { User } from './User';

export interface Request {
  id: number;
  title: string;
  slug: string;
  description: string;
  date_filed: Date;
  date_finished: Date;
  status: string;
  created_at: Date;
  updated_at: Date;
  user: User;
  organization: Organisation;
  communications: Communication[];
  notifications: Notification[];
  comments: Comment[];
  files: File[];
}
