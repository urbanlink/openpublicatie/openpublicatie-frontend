export interface User {
  id: number;
  email: string;
  auth0: string;
}
