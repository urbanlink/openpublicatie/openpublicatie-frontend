import { initAuth0 } from '@auth0/nextjs-auth0';
// import { createContext, useContext, useState } from 'react';
import config from './config';

// https://github.com/auth0/nextjs-auth0/issues/69#issuecomment-596760312
const auth0Config = {
  clientId: config.AUTH0_CLIENT_ID,
  clientSecret: config.AUTH0_CLIENT_SECRET,
  scope: config.AUTH0_SCOPE,
  domain: config.AUTH0_DOMAIN,
  redirectUri: config.REDIRECT_URI,
  postLogoutRedirectUri: config.POST_LOGOUT_REDIRECT_URI,
  audience: config.AUTH0_AUDIENCE,
  session: {
    cookieSecret: config.SESSION_COOKIE_SECRET,
    cookieLifetime: Number(config.SESSION_COOKIE_LIFETIME),
    storeIdToken: false,
    storeRefreshToken: false,
    storeAccessToken: true,
  },
};

export default initAuth0(auth0Config);

// export const AuthContext = createContext({});

// export const AuthProvider = ({ children }: any) => {

//   const token = getTokenFromCookie(cookies);

//   // const dataToken = token ? jwt.decode(token) : {}; // extract data from token
//   const [dataAuth, setDataAuth] = useState(
//     getUser() || { token, isLogin: !!token, ...dataToken }
//   );

//   return (
//     <AuthContext.Provider value={[dataAuth, setDataAuth]}>
//       {children}
//     </AuthContext.Provider>
//   );
// };

// export const useAuth = () => useContext(AuthContext);
