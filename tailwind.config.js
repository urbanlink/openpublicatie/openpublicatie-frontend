module.exports = {
  purge: ['./src/components/**/*.tsx', './src/pages/**/*.tsx'],
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    darkSelector: '.dark-mode',
    container: {
      center: true,
      padding: '1rem',
    },
    fontFamily: {
      header: ['Roboto Slab', 'sans-serif'],
      body: ['Alegreya', 'serif'],
    },
    extend: {
      colors: {
        primary: {
          default: '#2f4a73',
        },
        secondary: {
          default: '#fd6558',
        },
        background: {
          default: '#e5ded3',
          light: '#f6f3ef',
        },
        brown: {
          light: '#beb39f',
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
